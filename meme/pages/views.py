from django.shortcuts import redirect, render
from meme.pages.forms import SignupForm


def home(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            # form.save()
            return redirect('home')
        else:
            print form.errors['password2']
    else:
        form = SignupForm()
    return render(request, 'pages/home.html', {
        'signup_form': form
    })


def divide_by_zero(request):
    """Good for testing 500 error handler when set DEBUG=False."""
    1987 / 0