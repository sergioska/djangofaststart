from django import forms
from django.utils.translation import ugettext as _
from meme.pages.countries import COUNTRIES


class SignupForm(forms.Form):
    first_name = forms.CharField(label=_('First name'), max_length=30)
    last_name = forms.CharField(label=_('Last name'), max_length=30)
    email = forms.EmailField(label=_('Email'))
    country = forms.ChoiceField(label=_("Country"), choices=COUNTRIES)
    about = forms.CharField(label=_('About'), widget=forms.Textarea)
    password1 = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as above, for verification."))

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(_("The two password fields didn't match."))
        return password2
