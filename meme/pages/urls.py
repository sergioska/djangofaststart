from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from meme.pages import views


urlpatterns = patterns('',
    url(r'^$', views.home, name='home'),

    url(r'^divide_by_zero_please/$', views.divide_by_zero) # to test 500 error handler
)